using System;
using System.Reflection;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Assemblies.GAC.Dto {
	[Dto, Serializable]
	public class GacAssembly {
		public string CultureString { get; set; }
		public string FullName { get; set; }
		public string CompleteName { get; set; }
		public AssemblyName AssemblyName { get; set; }
	}
}