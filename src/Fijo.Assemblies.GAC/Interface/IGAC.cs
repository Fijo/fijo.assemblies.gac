using System.Collections.Generic;
using Fijo.Assemblies.GAC.Dto;

namespace Fijo.Assemblies.GAC.Interface {
	public interface IGAC {
		IEnumerable<GacAssembly> Get();
	}
}