using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using Fijo.Assemblies.GAC.Dto;
using Fijo.Assemblies.GAC.Interface;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Assemblies.GAC.Services {
	[PublicAPI]
	[RelatedLink("helpfull information about stuff in private classes/ members in this class",
		"http://code.google.com/p/simple-assembly-explorer/source/browse/trunk/SimpleAssemblyExplorer.Core/Common/FusionNative.cs?spec=svn435&r=435",
		"http://blogs.msdn.com/b/junfeng/archive/2004/09/14/229649.aspx",
		"http://geekswithblogs.net/dbose/archive/2008/11/07/recreate-gacutil---part-2.aspx",
		"http://msdn.microsoft.com/de-de/library/windows/desktop/aa375163(v=vs.85).aspx",
		"https://github.com/Reactive-Extensions/IL2JS/blob/master/CCI/AssemblyCache.cs")]
	public class GAC : IGAC {
		private const int AssemblyInfoPathBufferSize = 512;

		[PublicAPI]
		public virtual IEnumerable<GacAssembly> Get() {
			return GetGacNameEntries().AsParallel().Select(MapGacAssembly);
		}

		private GacAssembly MapGacAssembly(IAssemblyName currentAssembly) {
			var cultureString = GetPropertyString(currentAssembly, AssemblyNameProperties.Culture);
			var assemblyName = GetDisplayName(currentAssembly, AssemblyNameDisplayFlags.All);
			var assemblyInfo = QueryAssemblyInfo(assemblyName, AssemblyInfoQueryFlags.Validate);
			return new GacAssembly
			{
				FullName = GetDisplayName(currentAssembly),
				CompleteName = assemblyName,
				CultureString = cultureString,
				AssemblyName = MapAssemblyName(currentAssembly, cultureString, assemblyInfo),
			};
		}

		private AssemblyName MapAssemblyName([NotNull] IAssemblyName currentAssembly, string cultureString, AssemblyInfo assemblyInfo) {
			var assemblyName = new AssemblyName
			{
				Name = GetPropertyString(currentAssembly, AssemblyNameProperties.Name),
				Version = MapVersion(currentAssembly),
				CultureInfo = CultureInfo.GetCultureInfo(cultureString),
				CodeBase = assemblyInfo.AssemblyPath,
				ProcessorArchitecture = GetProcessorArchitecture(currentAssembly),
			};
			assemblyName.SetPublicKey(GetPropertyBytes(currentAssembly, AssemblyNameProperties.PublicKey));
			assemblyName.SetPublicKeyToken(GetPropertyBytes(currentAssembly, AssemblyNameProperties.PublicKeyToken));
			return assemblyName;
		}

		private ProcessorArchitecture GetProcessorArchitecture([NotNull] IAssemblyName currentAssembly) {
			var architectureDisplayName = GetDisplayName(currentAssembly, AssemblyNameDisplayFlags.ProcessorArchitecture);
			var equalSeperatorIndex = architectureDisplayName.LastIndexOf('=');
			if (equalSeperatorIndex == -1) return ProcessorArchitecture.None;
			Debug.Assert(equalSeperatorIndex + 1 < architectureDisplayName.Length);
			var architecture = architectureDisplayName.Substring(equalSeperatorIndex + 1);
			return (ProcessorArchitecture) Enum.Parse(typeof (ProcessorArchitecture), architecture, true);
		}

		private Version MapVersion([NotNull] IAssemblyName currentAssembly) {
			return new Version(GetPropertyUInt16(currentAssembly, AssemblyNameProperties.MajorVersion),
			                   GetPropertyUInt16(currentAssembly, AssemblyNameProperties.MinorVersion),
			                   GetPropertyUInt16(currentAssembly, AssemblyNameProperties.BuildNumber),
			                   GetPropertyUInt16(currentAssembly, AssemblyNameProperties.RevisionNumber));
		}

		#region AssemblyName (service) methods
		private string GetDisplayName([NotNull] IAssemblyName assemblyName, AssemblyNameDisplayFlags nameDisplayFlags = AssemblyNameDisplayFlags.Full) {
			return GetDisplayNameByNamePointer(assemblyName, nameDisplayFlags, GetDisplayNamePointer(assemblyName, nameDisplayFlags));
		}

		private AssemblyInfo QueryAssemblyInfo([NotNull] string assemblyName, AssemblyInfoQueryFlags assemblyInfoQueryFlags = AssemblyInfoQueryFlags.None) {
			if (assemblyName == null) throw new ArgumentNullException("assemblyName");
			var assemblyInfo = GetEmptyAssemblyInfo();
			var assemblyCache = GetAssemblyCache();
			var result = assemblyCache.QueryAssemblyInfo((int) assemblyInfoQueryFlags, assemblyName, ref assemblyInfo);
			if (result != IntPtr.Zero) throw new ArgumentException(string.Format("QueryAssemblyInfo faild for �{0}�", assemblyName), "assemblyName");
			return assemblyInfo;
		}

		#region GetProperty methods
		private string GetPropertyString([NotNull] IAssemblyName assemblyName, AssemblyNameProperties assemblyNameProperty) {
			return GetProperty(assemblyName, assemblyNameProperty, () => string.Empty, (ptr, size) => Marshal.PtrToStringUni(ptr));
		}

		private ushort GetPropertyUInt16([NotNull] IAssemblyName assemblyName, AssemblyNameProperties assemblyNameProperty) {
			return GetProperty(assemblyName, assemblyNameProperty, null, (ptr, size) => (ushort) Marshal.ReadInt16(ptr));
		}

		private byte[] GetPropertyBytes([NotNull] IAssemblyName assemblyName, AssemblyNameProperties assemblyNameProperty) {
			return GetProperty(assemblyName, assemblyNameProperty, null, (ptr, size) => {
				var value = new byte[(int) size];
				Marshal.Copy(ptr, value, 0, (int) size);
				return value;
			});
		}
		#endregion

		#region Helper methods
		#region for GetDisplayName
		private uint GetDisplayNamePointer([NotNull] IAssemblyName assemblyName, AssemblyNameDisplayFlags nameDisplayFlags) {
			var pccDisplayName = 0U;
			assemblyName.GetDisplayName(IntPtr.Zero, ref pccDisplayName, (uint) nameDisplayFlags);
			if (pccDisplayName == 0U) throw new InvalidOperationException("Faild to get a pointer to the AssemblyDisplayName.");
			return pccDisplayName;
		}

		private unsafe string GetDisplayNameByNamePointer([NotNull] IAssemblyName assemblyName, AssemblyNameDisplayFlags nameDisplayFlags, uint pccDisplayName) {
			fixed (byte* numPtr = new byte[(uint) (((int) pccDisplayName + 1) * 2)]) {
				var num = new IntPtr(numPtr);
				assemblyName.GetDisplayName(num, ref pccDisplayName, (uint) nameDisplayFlags);
				return Marshal.PtrToStringUni(num);
			}
		}
		#endregion

		#region for QueryAssemblyInfo
		private AssemblyInfo GetEmptyAssemblyInfo() {
			return new AssemblyInfo
			{
				PathBufferSize = AssemblyInfoPathBufferSize,
				AssemblyPath = new string('\0', AssemblyInfoPathBufferSize)
			};
		}

		[NotNull]
		private IAssemblyCache GetAssemblyCache() {
			IAssemblyCache assemblyCache;
			var result = CreateAssemblyCache(out assemblyCache, 0);
			if (result != IntPtr.Zero) Marshal.ThrowExceptionForHR(0, result);
			return assemblyCache;
		}
		#endregion

		#region for GetProperty methods
		private T GetProperty<T>([NotNull] IAssemblyName assemblyName, AssemblyNameProperties assemblyNameProperty, [CanBeNull] Func<T> emptySizeAbortValueResolver, [NotNull] Func<IntPtr, uint, T> valueResolver) {
			var size = GetPropertyValueSize(assemblyName, assemblyNameProperty);
			if (emptySizeAbortValueResolver != null && size == 0) return emptySizeAbortValueResolver();
			var ptr = Marshal.AllocHGlobal((int) size);
			T result;
			try {
				assemblyName.GetProperty((uint) assemblyNameProperty, ptr, ref size);
				result = valueResolver(ptr, size);
			}
			finally {
				Marshal.FreeHGlobal(ptr);
			}
			return result;
		}

		private uint GetPropertyValueSize(IAssemblyName assemblyName, AssemblyNameProperties assemblyNameProperty) {
			var size = 0U;
			assemblyName.GetProperty((uint) assemblyNameProperty, IntPtr.Zero, ref size);
			return size;
		}
		#endregion
		#endregion
		#endregion

		#region AssemblyName provider methods
		private IEnumerable<IAssemblyName> GetGacNameEntries() {
			IAssemblyEnum enumeration;
			HandleException(CreateAssemblyEnum(out enumeration, null, null, 2U, 0));
			IApplicationContext context;
			IAssemblyName assemblyName;
			while (enumeration.GetNextAssembly(out context, out assemblyName, 0U) == 0)
				yield return assemblyName;
		}
		#endregion

		#region AssemblyNameDisplayFlags
		[Flags]
		[RelatedLink("http://msdn.microsoft.com/de-de/library/vstudio/ms232947(v=vs.100).aspx")]
		private enum AssemblyNameDisplayFlags {
			Version = 0x01,
			Culture = 0x02,
			PublicKeyToken = 0x04,
			PublicKey = 0x08,
			Custom = 0x10,
			ProcessorArchitecture = 0x20,
			Languageid = 0x40,
			Retarget = 0x80,
			ConfigMask = 0x100,
			Mvid = 0x200,
			Full = Version | Culture | PublicKeyToken | Retarget,
			All = Version | Culture | PublicKeyToken | PublicKey | Custom | ProcessorArchitecture | Languageid | Retarget | ConfigMask | Mvid
		}
		#endregion

		#region AssemblyInfoQueryFlags
		[Flags]
		[RelatedLink("this enum is called dwFlags on the page.", "http://msdn.microsoft.com/de-de/library/windows/desktop/aa375163(v=vs.85).aspx")]
		private enum AssemblyInfoQueryFlags {
			None = 0,
			[Desc("include for check assembly hash and strong name.")] [About("Validate")] Validate = 1,
			[Desc("set size to all files in the AssemblyInfo. ?!")] GetSize = 2
		}
		#endregion

		#region AssemblyNameProperties
		[RelatedLink("http://code.google.com/p/simple-assembly-explorer/source/browse/trunk/SimpleAssemblyExplorer.Core/Common/FusionNative.cs?spec=svn435&r=435")]
		[RelatedLink("this enum is called ASM_NAME on the page.", "https://github.com/Reactive-Extensions/IL2JS/blob/master/CCI/AssemblyCache.cs")]
		private enum AssemblyNameProperties : uint {
			PublicKey = 0,
			PublicKeyToken = 1,
			HashValue = 2,
			Name = 3,
			MajorVersion = 4,
			MinorVersion = 5,
			BuildNumber = 6,
			RevisionNumber = 7,
			Culture = 8,
			ProcessorIDArray = 9,
			OsinfoArray = 10,
			HashAlgid = 11,
			Alias = 12,
			CodebaseUrl = 13,
			CodebaseLastmod = 14,
			NullPublicKey = 15,
			NullPublicKeyToken = 16,
			Custom = 17,
			NullCustom = 18,
			Mvid = 19,
			Only32Bit = 20
		}
		#endregion

		#region AssemblyInfo
		[StructLayout(LayoutKind.Sequential)]
		private struct AssemblyInfo {
			[Note("size of this structure for future expansion")] public int AssemblyInfoSize;
			public int AssemblyFlags;
			[Note("in KB")] public long AssemblySize;
			[MarshalAs(UnmanagedType.LPWStr)] public String AssemblyPath;
			[Note("size of path bufffer")] public int PathBufferSize;
		}
		#endregion

		#region AssemblyCacheUninstallDisposition
		private enum AssemblyCacheUninstallDisposition {
			Unknown = 0,
			Uninstalled = 1,
			StillInUse = 2,
			AlreadyUninstalled = 3,
			DeletePending = 4,
			HasInstallReference = 5,
			ReferenceNotFound = 6
		}
		#endregion

		#region InstallReference
		[StructLayout(LayoutKind.Sequential), UsedImplicitly]
		private class InstallReference {
			public InstallReference(Guid guid, string id, string data) {
				CbSize = 2 * IntPtr.Size + 16 + (id.Length + data.Length) * 2;
				GuidScheme = guid;
				Identifier = id;
				Description = data;
			}

			public int CbSize;
			public int Flags;
			public Guid GuidScheme;
			[MarshalAs(UnmanagedType.LPWStr)] public String Identifier;
			[MarshalAs(UnmanagedType.LPWStr)] public String Description;
		}
		#endregion

		#region IAssemblyCache
		[Guid("e707dcde-d1cd-11d2-bab9-00c04f8eceae")]
		[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IAssemblyCache {
			[PreserveSig]
			int UninstallAssembly(int flags, [MarshalAs(UnmanagedType.LPWStr)] string assemblyName, InstallReference refData, out AssemblyCacheUninstallDisposition disposition);

			[PreserveSig]
			IntPtr QueryAssemblyInfo([Note("AssemblyInfoQueryFlags")] int flags, [MarshalAs(UnmanagedType.LPWStr)] string assemblyName, ref AssemblyInfo assemblyInfo);

			[PreserveSig]
			int CreateAssemblyCacheItem(int flags, IntPtr pvReserved, [Note("IRegisterItem")] out object ppAsmItem, [MarshalAs(UnmanagedType.LPWStr)] string assemblyName);

			[PreserveSig]
			int CreateAssemblyScavenger(out object ppAsmScavenger);

			[PreserveSig]
			int InstallAssembly(int flags, [MarshalAs(UnmanagedType.LPWStr)] string assemblyFilePath, InstallReference refData);
		}
		#endregion

		#region IAssemblyEnum
		[Guid("21B8916C-F28E-11D2-A473-00C04F8EF448")]
		[SuppressUnmanagedCodeSecurity]
		[InterfaceType((short) 1)]
		[ComImport]
		private interface IAssemblyEnum {
			[MethodImpl(MethodImplOptions.PreserveSig)]
			int GetNextAssembly(out IApplicationContext context, out IAssemblyName assemblyName, uint dwFlags);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int Reset();

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int Clone(out IAssemblyEnum ppEnum);
		}
		#endregion

		#region IAssemblyName
		[SuppressUnmanagedCodeSecurity]
		[InterfaceType((short) 1)]
		[Guid("CD193BC0-B4BC-11D2-9833-00C04FC31D2E")]
		[ComImport]
		private interface IAssemblyName {
			[MethodImpl(MethodImplOptions.PreserveSig)]
			int SetProperty(uint propertyId, IntPtr pvProperty, uint cbProperty);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int GetProperty(uint propertyId, IntPtr pvProperty, ref uint pcbProperty);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			#pragma warning disable 465
			int Finalize();
			#pragma warning restore 465

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int GetDisplayName(IntPtr szDisplayName, ref uint pccDisplayName, uint dwDisplayFlags);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int BindToObject(object refIid, object pAsmBindSink, IApplicationContext pApplicationContext, [MarshalAs(UnmanagedType.LPWStr)] string szCodeBase, long llFlags, int pvReserved, uint cbReserved, out int ppv);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int GetName(ref uint lpcwBuffer, [MarshalAs(UnmanagedType.LPWStr), Out] StringBuilder pwzName);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int GetVersion(out uint pdwVersionHi, out uint pdwVersionLow);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int IsEqual(IAssemblyName pName, uint dwCmpFlags);

			[MethodImpl(MethodImplOptions.PreserveSig)]
			int Clone(out IAssemblyName pName);
		}
		#endregion

		#region IApplicationContext
		[InterfaceType((short) 1)]
		[SuppressUnmanagedCodeSecurity]
		[Guid("7C23FF90-33AF-11D3-95DA-00A024A85B51")]
		[ComImport]
		private interface IApplicationContext {
			void SetContextNameObject(IAssemblyName assemblyName);

			void GetContextNameObject(out IAssemblyName assemblyName);

			void Set([MarshalAs(UnmanagedType.LPWStr)] string szName, int pvValue, uint cbValue, uint dwFlags);

			void Get([MarshalAs(UnmanagedType.LPWStr)] string szName, out int pvValue, ref uint pcbValue, uint dwFlags);

			void GetDynamicDirectory(out int wzDynamicDir, ref uint pdwSize);
		}
		#endregion

		#region Marshal
		private void HandleException(int hResult) {
			if (hResult < 0) Marshal.ThrowExceptionForHR(hResult);
		}
		#endregion

		#region Extern
		[DllImport("fusion.dll")]
		private static extern IntPtr CreateAssemblyCache(out IAssemblyCache ppAsmCache, int reserved);

		[DllImport("Fusion.dll", CharSet = CharSet.Auto)]
		private static extern int CreateAssemblyEnum(out IAssemblyEnum assemblyEnum, IApplicationContext applicationContext, IAssemblyName assemblyName, uint flags, int reserved);
		#endregion
	}
}