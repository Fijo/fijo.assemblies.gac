﻿using Fijo.Assemblies.GAC.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.Assemblies.GACTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new GACInjectionModule());
		}
	}
}