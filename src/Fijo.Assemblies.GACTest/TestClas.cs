using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Fijo.Assemblies.GAC.Dto;
using Fijo.Assemblies.GAC.Interface;
using Fijo.Assemblies.GACTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.Assemblies.GACTest {
	[TestFixture]
	public class TestClas {
		private IGAC _gac;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_gac = Kernel.Resolve<IGAC>();
		}
		
		[Test]
		public void GetAllGacEntries_ContainsAtLeast1MsCoreLib() {
			var mscorelibs = MsCorLibEntries();
			Assert.GreaterOrEqual(mscorelibs.Count(), 1);
		}
		
		[Test]
		public void GetAllGacEntries_MsCoreLibEntryIsCorrectlyFilled() {
			var mscorelib = MsCorLibEntries().First();
			Assert.IsNotNull(mscorelib);
			var fullName = mscorelib.FullName;
			var completeName = mscorelib.CompleteName;
			var assemblyName = mscorelib.AssemblyName;
			Assert.IsNotEmpty(fullName);
			Assert.IsNotEmpty(completeName);
			Assert.GreaterOrEqual(completeName.Length, fullName.Length);
			Assert.AreNotEqual(0, assemblyName.Version.Major);
			var value = Enum.GetName(typeof (ProcessorArchitecture), assemblyName.ProcessorArchitecture);
			Assert.NotNull(value);
			Assert.IsTrue(completeName.ToLowerInvariant().Contains(value.ToLowerInvariant()));
			Assert.IsTrue(fullName.Contains(assemblyName.Version.ToString()));
			Assert.IsTrue(fullName.Contains(assemblyName.Name));
			Assert.AreEqual(fullName, assemblyName.FullName);
			Assert.IsNotEmpty(mscorelib.AssemblyName.CodeBase);
			Assert.True(File.Exists(mscorelib.AssemblyName.CodeBase));
			Assert.NotNull(assemblyName.CultureInfo);
			Assert.IsNotEmpty(assemblyName.Name);
		}

		private IEnumerable<GacAssembly> MsCorLibEntries() {
			return GetAllGacEntries().Where(x => x.FullName.Contains("mscorlib"));
		}

		private IEnumerable<GacAssembly> GetAllGacEntries() {
			return _gac.Get();
		}
	}
}